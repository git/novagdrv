
	NOVAG CHESS COMPUTER DRIVER

This is a user space driver for Novag Chess Computers.
A Python program is included to play on Lichess. Also, patches are provided for
ChessDB and EBoard.

Lichess and EBoard allow you to play games with other people over the Internet,
and of course, you may play using your Novag chess computer! The LEDs on the
board show your opponent's moves. You may also play against an engine.

ChessDB is great to analyze your games. You may introduce a game, go
backwards, introduce or try variants, and you may do all this from your
Novag chess computer!

Using a Novag chess computer as a DGT board works quite well but it's tricky in
some situations. Make sure you read the 'Known issues' section.


Supported chess computers
=========================

- Novag Citrine
- Novag Star Diamond

Please, if you own another Novag chess computer, contact me, whether it
works or not. Thanks!


Get the driver working
======================

- To compile it, just run make.

- Copy novagdrv and novagdrv-lichess.py somewhere in your path (eg.
  /usr/local/bin or ~/.local/bin).

- If the COM device you are using is not /dev/ttyS0, then create a file
  named .novagdrvrc in your home directory. Insert something like
  "device=/dev/ttyUSB0" (without quotes).

- Test it. Connect your chess computer. Run novagdrv. Make some moves on the
  board. You should see the moves on the terminal. If not, you might have
  introduced a wrong device in ~/.novagdrvrc.

- Kill the driver (control+C).

- Now, jump to the "LICHESS", "CHESSDB" or "EBOARD" section!


LICHESS
=======

The included novagdrv-lichess.py Python program (based on certabo-lichess)
allows you to play on Lichess using your Novag chess computer.

- Generate an API access token on Lichess (Preferences -> API Access tokens).
  Make sure you enable the "Play games with the board API" scope.
  Write it to ~/.lichess.token (or use the --token command line option).

- Install berserk (Python client for the Lichess API):

    pip install berserk

- Run novagdrv-lichess.py; create a game on Lichess and start playing right
  away!

- Correspondence games are skipped by default. Use --correspondence to
  override.

- Note not all games can be played using the board API. See restrictions at:

    https://lichess.org/api#tag/Board


CHESSDB
=======

- You need Tcl/Tk (version 8.0 or higher) development packages.
  For example, on Debian or Ubuntu, install tcl8.4-dev and tk8.4-dev.

- Download ChessDB 3.6.18 (the patch may also work with newer versions).

- Uncompress it:

    tar xzvf ChessDB-3.6.18.tar.gz

- Apply the patch:

    cd ChessDB-3.6.18
    patch -p1 < .../ChessDB-3.6.18-novagdrv.patch
    (replace "..." with the path to novagdrv source)

- Compile it:

    ./configure
    make
    sudo make install
    (if sudo does not work, become root and run make install)

- Run chessdb. Click on Tools -> Novag Chess Computer. Enjoy!


EBOARD
======

- You need GTK2 development packages.
  For example, on Debian or Ubuntu, install libgtk2.0-dev.

- Download eboard-1.1.1.tar.bz2 and eboard-extras-2.tar.gz from:

    http://sourceforge.net/project/showfiles.php?group_id=11164

- Uncompress them:

    tar xjvf eboard-1.1.1.tar.bz2
    tar xzvf eboard-extras-2.tar.gz

- Apply the patch:

    cd eboard-1.1.1
    patch -p1 < .../eboard-1.1.1-novagdrv.patch
    (replace "..." with the path to novagdrv source)

- Compile it:

    ./configure
    make
    sudo make install
    (if sudo does not work, become root and run make install)

- Install extras:

    cd ../eboard-extras-2
    ./configure
    sudo make install

- Run:

    eboard -novagport /dev/ttyS0
    (change ttyS0 for your device)

- Connect to FICS. Prepare your board and issue a command like this:

    seek 20 0 white
    (change white for black if you want to play with black pieces)

  Wait until FICS finds an opponent for you :-)


Known issues
============

These machines are not designed to be used the way we are using them, so this
driver is implemented by using a lot of tricks. So, there are some issues which
are difficult, if possible, to fix.

- When your opponent captures a piece, the destination square will only blink
  for a moment. If you miss it, you may have to look at the screen.

- It takes one second or two from the time your opponent moves until the LEDs
  begin to blink, so I don't suggest playing very fast games.

- When your opponent moves, you will see an error on the LCD. Just ignore it.
  This error is the trick we use to make LEDs blink, and if you think for
  a while, you'll notice that this explains both previous issues :-)

- When you are checkmated, the final move is not shown, so you don't know
  the game is over. I recommend enabling sound events in EBoard.

- To play with black pieces, you have to turn your chess computer around.


Contributing
============

Contact me at:

    Maximiliano Pin <mxcpin --AT-- gmail --DOT-- com>


Distribution
============

  Git repository and new releases may be found at:

    http://savannah.gnu.org/projects/novagdrv


Thanks
======

- Cristina Sancho: For giving me my cool Novag Citrine for my 27th birthday!
  For help with testing and for her great patience.

- Michael L. Windham: For helping me to make Novag Star Diamond work with
  this driver, and for documentation review.


Copyright
=========

  Copyright (C) 2007-2020 Maximiliano Pin

  This is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

