/* game.cc -- stores the game
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "novag.h"

bool Game::add_move(int num, player_t player, cstr move) {
  if (player == get_move_player() || player == BLACK && num != get_move_num() ||
      player == WHITE && num != (get_move_num() + 1)) {
    // this is not the move we were expecting
    return false;
  }
  else {
    positions.push_back(Position(pos(), move));
    return true;
  }
}

void Game::add_move(cstr move) {
  positions.push_back(Position(pos(), move));
}

void Game::take_back() {
  positions.pop_back();
}

void Game::reset() {
  positions.clear();
  positions.push_back(Position());  // initial position
}
