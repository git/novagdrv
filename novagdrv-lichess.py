#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import select
import signal
import subprocess
import sys
import threading
import time

import berserk


parser = argparse.ArgumentParser()
parser.add_argument("--token", default=os.path.expanduser("~/.lichess.token"), help='token file path')
parser.add_argument("--device", help='serial port device (e.g. /dev/ttyUSB0)')
parser.add_argument("--correspondence", action="store_true", help='do not skip correspondence games')
parser.add_argument("--debug", action="store_true", help='enable debug')
parser.add_argument("--debugdrv", action="store_true", help='enable driver debug')
parser.add_argument("--devmode", action="store_true", help='development mode')
args = parser.parse_args()


def signal_handler(signal, frame):
    sys.exit(0)


def debug(msg):
    if args.debug:
        print(msg)


class Novag(object):
    def __init__(self):
        self._driver = None


    def start(self):
        if self._driver:
            return
        debug('starting driver')
        cmd = ['novagdrv', '-u', '-U']
        if args.device:
            cmd += ['-f', args.device]
        if args.debugdrv:
            cmd += ['-d']
        self._driver = subprocess.Popen(cmd, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE)


    def stop(self):
        if not self._driver:
            return
        debug('stopping driver')
        self._driver.stdin.write(('CLOSE\n').encode('utf-8'))
        self._driver.stdin.flush()
        try:
            self._driver.wait(2)
            debug('driver stopped')
        except TimeoutExpired:
            debug('timeout waiting for driver to finish')
            self._driver.kill()
            self._driver.wait(2)
        self._driver = None


    def reset(self):
        debug('resetting driver')
        self.stop()
        self.start()


    def send_move(self, move):
        self._driver.stdin.write((move + '\n').encode('utf-8'))
        self._driver.stdin.flush()


    def send_moves(self, moves):
        if len(moves) == 1:
            return self.send_move(moves[0])
        self._driver.stdin.write(('MOVES\n').encode('utf-8'))
        for move in moves:
            self._driver.stdin.write((move + '\n').encode('utf-8'))
        self._driver.stdin.write(('END\n').encode('utf-8'))
        self._driver.stdin.flush()


    def read_move(self):
        line = self._driver.stdout.readline()
        if not line:
            print('ERROR: EOF in driver output')
            sys.exit(1)
        debug(f'read from driver: {line}')
        line = line.decode('utf-8').rstrip()
        return line


    def wait_for_new(self):
        while self.read_move() != 'NEW':
            pass


    def get_game(self):
        moves = []
        poll_obj = select.poll()
        poll_obj.register(self._driver.stdout, select.POLLIN)
        while True:
            poll_result = poll_obj.poll(500)
            if poll_result:
                moves.append(self.read_move())
            else:
                break
        return moves


class GameError(RuntimeError):
    pass

class Game(threading.Thread):
    def __init__(self, client, driver, game_id, **kwargs):
        super().__init__(**kwargs)
        self.daemon = True
        self.game_id = game_id
        self.driver = driver
        self.client = client
        self.stream = client.board.stream_game_state(game_id)
        self.current_state = next(self.stream)
        self.moves = []
        self.lichess_game = None
        self.color = None

        self.fetch_lichess_game()

        if not self.lichess_game:
            raise GameError('cannot find game on Lichess')

        driver.reset()
        self.color = self.lichess_game['color']
        print(f"Switched to game '{self.game_id}', you play with {self.color}. Have fun!")


    def fetch_lichess_game(self):
        self.lichess_game = None
        for game in self.client.games.get_ongoing():
            if game['gameId'] == self.game_id:
                debug(f'game: {game}')
                self.lichess_game = game
        if not self.lichess_game:
            debug('cannot find game on Lichess')


    def run(self):
        debug(f'getting game from board...')
        self.moves = self.driver.get_game()
        debug(f'game on board: {self.moves}')

        last_lichess_move = self.lichess_game['lastMove']
        if not last_lichess_move and len(self.moves) > 0:
            print('NEW GAME, please set initial position on board.')
            self.driver.wait_for_new()
            self.moves = []
            print('Thanks!')

        # TODO check if it's the same game using fen (maybe + last_lichess_move)
        if last_lichess_move and (not self.moves or self.moves[-1] != last_lichess_move):
            # we can't get the game until a move is made on Lichess... :-(
            print('BOARD OUT OF SYNC!')
            if self.lichess_game['isMyTurn']:
                print('Please make a move on Lichess.')
        elif self.lichess_game['isMyTurn']:
            self.move_from_board()

        print('Waiting for move from Lichess...')
        for event in self.stream:
            if event['type'] == 'gameState':
                self.handle_state_change(event)
            elif event['type'] == 'chatLine':
                self.handle_chat_line(event)
        print(f"Game ended: {self.status}")


    def move_from_board(self):
        initial_moves = self.moves[:]
        while len(self.moves) <= len(initial_moves):
            print('Waiting for move from board...')
            move = self.driver.read_move()
            if move == 'NEW':
                print('Board has been set to initial position.')
                self.moves = []
                # if the current game finished in our turn, we can set the
                # initial position to get out of the current game
                self.fetch_lichess_game()
                if not self.lichess_game:
                    return
            elif move == 'TAKEBACK':
                print('Takeback on board.')
                self.moves.pop()
            else:
                print(f'Move from board: {move}')
                self.moves.append(move)

        if self.moves[:-1] != initial_moves:
            print('ERROR: You got the board out of sync, move on Lichess.')
            return

        for attempt in range(3):
            try:
                self.client.board.make_move(self.game_id, move)
                debug('make_move succeeded')
                return
            except Exception as e:
                debug(f'exception on make_move: {e}')
            if attempt > 1:
                debug(f'sleeping before retry')
                time.sleep(3)


    def is_my_turn(self):
        return (self.color == 'black') == bool(len(self.moves) % 2)


    def handle_state_change(self, game_state):
        debug(game_state)
        self.status = game_state['status']
        moves_update = game_state['moves'].split(' ')
        if moves_update == self.moves:
            debug('no new move')
            return
        elif moves_update[:len(self.moves)] == self.moves:
            new_moves = moves_update[len(self.moves):]
            print(f'New move(s) from Lichess: {new_moves}')
            self.driver.send_moves(new_moves)
            self.moves = moves_update
            if self.status == 'started' and self.is_my_turn():
                self.move_from_board()
        elif self.status == 'started':
            print('DIFFERENT GAME, please set initial position on board.')
            debug(f'game on board:   {self.moves}')
            debug(f'game on Lichess: {moves_update}')
            if self.moves:
                self.driver.wait_for_new()
            self.driver.send_moves(moves_update)
            self.moves = moves_update
            print('OK, now copy the current position from the screen.')
            if self.is_my_turn():
                print('... And make your move.')
                self.move_from_board()
        print('Waiting for move from Lichess...')


    def handle_chat_line(self, chat_line):
        debug(f'chat: {chat_line}')


def main():
    signal.signal(signal.SIGINT, signal_handler)

    driver = Novag()
    driver.start()

    try:
        with open(args.token) as f:
            token = f.read().strip()
    except FileNotFoundError:
        print(f'ERROR: cannot find token file')
        sys.exit(1)
    except PermissionError:
        print(f'ERROR: permission denied on token file')
        sys.exit(1)

    try:
        session = berserk.TokenSession(token)
    except:
        e = sys.exc_info()[0]
        print(f"ERROR: cannot create session: {e}")
        sys.exit(-1)

    try:
        if args.devmode:
            client = berserk.Client(session, base_url="https://lichess.dev")
        else:
            client = berserk.Client(session)
    except:
        e = sys.exc_info()[0]
        print(f"ERROR: cannot create Lichess client: {e}")
        sys.exit(-1)

    print('Connected to Lichess.')

    def is_correspondence(gameId):
        try:
            for game in client.games.get_ongoing():
                if game['gameId'] == gameId:
                    if game['speed'] == "correspondence":
                        return True
        except:
            e = sys.exc_info()[0]
            print(f"cannot determine game speed: {e}")
        return False

    game = None
    while True:
        try:
            for event in client.board.stream_incoming_events():
                if event['type'] == 'challenge':
                    print("Challenge received.")
                    debug(event)
                elif event['type'] == 'gameStart':
                    if game:
                        game.join()
                        game = None

                    game_data = event['game']
                    debug(f"game start received: {game_data['id']}")

                    # check if game speed is correspondence, skip those if --correspondence argument is not set
                    if not args.correspondence:
                        if is_correspondence(game_data['id']):
                            print(f"Skipping corespondence game: {game_data['id']}")
                            continue

                    try:
                        game = Game(client, driver, game_data['id'])
                        game.start()
                    except GameError as e:
                        print(f'ERROR: {e}')
                        continue
                    except berserk.exceptions.ResponseError as e:
                        cannotPlayText = 'This game cannot be played with the Board API.'
                        if cannotPlayText in str(e):
                            print(cannotPlayText)
                        else:
                            print(f'ERROR: {e}')
                        continue

        except berserk.exceptions.ResponseError as e:
            print(f'ERROR: Invalid server response: {e}')
            if 'Too Many Requests for url' in str(e):
                time.sleep(10)

if __name__ == '__main__':
    main()
