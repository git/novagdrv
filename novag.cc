/* novag.cc -- novag protocol
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "novag.h"

using namespace std;

void Novag::start() {
  if (referee_mode) {
    serial.send("U ON");

    // if we send commands too fast, they are ignored
    usleep(SERIAL_DELAY);
  }

  // this is needed at least by Star Diamond
  serial.send("X ON");
  usleep(SERIAL_DELAY);

  query_game();
}

void Novag::received_line(cstr line) {
  if (line.empty())
    return;

  if (line[0] == 'M') {
    received_move(line);
  }
  else if (line[0] == 'm') {
    // boards like Star Diamond use a different move format
    received_move(move_convert(line));
  }
  else if (line[0] == 'T') {
    cout << "TAKEBACK" << endl;
    game.take_back();
  }
  else if (line[0] == 'G') {
    if (state != RECV_GAME_HDR) {
      err_out("Unexpected G");
      return;
    }
    // queried game header
    vector<string> v;
    tokenize(line, &v);
    if (v.size() >= 2) {
      expected_moves = atoi(v[1].c_str());
      state = RECV_GAME;
    }
    else {
      dbg_out("There's no game.");
      state = NORMAL;
    }
  }
  else if (line[0] == '.') {
    if (state == RECV_GAME) {
      game_line(line);
    }
  }
  else if (line[0] == 'N') {
    cout << "NEW" << endl;
    game.reset();
    if (referee_mode) {
      serial.send("U ON");
      usleep(SERIAL_DELAY);
    }
  }
}

void Novag::reset_game() {
  game.reset();

  serial.send("N");
  usleep(SERIAL_DELAY);
  serial.expect('N');
}

void Novag::send_move(cstr move, bool blink) {
  game.add_move(move);

  // we send the move twice to activate leds;
  // we have to convert castle notation, because if we send
  // "O-O" or "O-O-O", the second send may be a valid move
  string s("m ");
  s.append(game.get_last_move().to_string(NotationNovagNoCastle));
  serial.send(s);
  if (blink) {
    usleep(SERIAL_DELAY);
    serial.send(s);
  }
}

void Novag::send_moves(const std::vector<std::string>& moves) {
  serial.send("U ON");
  usleep(SERIAL_DELAY);

  for (size_t i = 0; i < moves.size(); ++i) {
    bool blink = (i == moves.size() - 1);
    send_move(moves[i], blink);
    serial.expect('M');
  }

  if (!referee_mode) {
    serial.send("U OFF");
    usleep(SERIAL_DELAY);
  }
}

void Novag::query_game() {
  if (state != NORMAL) {
    err_out("query_game() receiving_game");
    return;
  }
  state = RECV_GAME_HDR;
  serial.send("G");
}

void Novag::received_move(cstr l) {
  if (l.size() > 1 && l[1] == '#') {
    dbg_out("Mate!");
    return;
  }

  vector<string> tkns;
  tokenize(l, &tkns);
  if (tkns.size() != 3) {
    cout << "!!! move line tokens != 3\n";
    return;
  }
  int num = atoi(tkns[1].c_str());
  player_t player = (tkns[1].find(',') != string::npos) ? BLACK : WHITE;
  if (num == game.get_move_num() && player == game.get_move_player()) {
    dbg_out("ignoring move echo");
    return;
  }
  if (!add_move(num, player, tkns[2])) {
    // workaround to recover missing moves
    dbg_out("missing move!");
    sleep(2);  // let it flash at least for 2 seconds...
               // (some better solution?)
    query_game();
  }
}

void Novag::game_line(cstr l) {
  vector<string> tkns;
  tokenize(l, &tkns);
  if (tkns.size() != 3 && tkns.size() != 4) {
    err_out("Game line tokens != 3 or 4");
    return;
  }
  if (tkns[1] == "white")  // ignore header
    return;

  int num = atoi(tkns[1].c_str());
  add_move(num, WHITE, tkns[2]);
  if (tkns.size() > 3) {
    add_move(num, BLACK, tkns[3]);
  }

  if (num == expected_moves) {
    dbg_out("end recv game");
    state = NORMAL;
  }
}

bool Novag::add_move(int num, player_t player, cstr move) {
  bool rv;
  if (true == (rv = game.add_move(num, player, move)))
    cout << game.get_last_move().to_string(notation) << endl;
  return rv;
}

string Novag::move_convert(string move) {
  string::size_type p = move.find("               ");
  if (p != string::npos) {
    move.erase(p, 15);
    move.insert(p, ",  ");
  }
  dbg_out("converted to: [" + move + "]");
  return move;
}
