PROG = novagdrv
OBJS = main.o novag.o serial.o game.o position.o
CC = $(CXX)
CXXFLAGS = -g
LDFLAGS = -g

$(PROG): $(OBJS)
	$(CXX) $(LDFLAGS) -o $(PROG) $(OBJS)

$(OBJS): Makefile novag.h

clean:
	rm -f $(PROG) $(OBJS)
