/* serial.cc -- serial port handling
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "novag.h"

#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;

Serial::~Serial() {
  close();
}

void Serial::open(cstr device, speed_t speed) {
  fd = ::open(device.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd < 0) {
    perror(device.c_str());
    throw runtime_error("Opening serial port.");
  }

  struct termios oldtio, newtio;
  tcgetattr(fd, &oldtio); /* save current port settings */

  memset(&newtio, 0, sizeof(newtio));
  newtio.c_cflag = CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set speed */
  cfsetispeed(&newtio, speed);
  cfsetospeed(&newtio, speed);

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  tcflush(fd, TCIFLUSH);           /* discard old input data */
  tcsetattr(fd, TCSANOW, &newtio); /* set attributes */
}

void Serial::close() {
  if (fd >= 0)
    ::close(fd);
  fd = -1;
}

void Serial::send(cstr line) {
  dbg_out(string("send: ") + "[" + line + "]");
  string buf(line + "\r\n");
  write(fd, buf.data(), buf.size());
}

void Serial::recv(string* line) {
  char c;
  do {
    int b;
    wait_readable();
    if ((b = read(fd, &c, 1)) == 1) {
      (*line) += c;
    }
    else if (b == 0) {
      err_out("EOF on serial!?");
    }
    else {
      perror("read");
      err_out("Serial::recv");
    }
  } while (c != '\n');

  line->resize(line->find_first_of("\r\n"));
  dbg_out("recv: [" + *line + "]");
}

void Serial::expect(const char c) {
  while (true) {
    std::string line;
    recv(&line);
    if (!line.empty() && line[0] == c)
      break;
  }
}

void Serial::wait_readable() {
  fd_set rfds;
  int retval;

  FD_ZERO(&rfds);
  FD_SET(fd, &rfds);
  retval = select(fd + 1, &rfds, NULL, NULL, NULL);
  if (retval < 0) {
    perror("select");
    err_out("SELECT ERROR");
    exit(1);
  }
}
