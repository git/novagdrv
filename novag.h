/* novag.h -- main header
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NOVAG_H
#define NOVAG_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <termios.h>
#include <unistd.h>
#include <vector>

typedef const std::string& cstr;
typedef bool player_t;
#define WHITE false
#define BLACK true

#define SERIAL_DELAY 100000

class Serial {
public:
  Serial() : fd(-1) {}
  ~Serial();
  void open(cstr device, speed_t speed);
  void close();
  void send(cstr line);
  void recv(std::string* line);
  void expect(const char c);
  void wait_readable();
  int get_fd() const { return fd; }

private:
  int fd;
};

enum Notation {
  NotationDefault,
  NotationDefaultNoCastle,
  NotationNovag,
  NotationNovagNoCastle,
  NotationNovagWithPiece,
  NotationUCI
};

struct Move {
  int ox;
  int oy;
  int dx;
  int dy;
  bool short_castle;
  bool long_castle;
  bool take;
  bool ep;
  char piece;
  char promote;
  Move()
    : ox(-1),
      oy(-1),
      dx(-1),
      dy(-1),
      short_castle(false),
      long_castle(false),
      take(false),
      ep(false),
      piece(' '),
      promote(' ') {}
  std::string to_string(Notation notation) const;
};

class Position {
public:
  Position();
  Position(const Position& prev, cstr move);
  const Move& get_last_move() const { return last_move; }
  int get_move_num() const { return move_num; }
  player_t get_move_player() const { return move_player; }
  void print() const;

private:
  typedef std::vector<std::pair<int, int>> square_set_t;

  void decode_move(cstr move, Move* smove);
  void deduce_move(Move* smove);
  void gen_origs(char piece, char rpiece, int x, int y, square_set_t* origs);
  void find(char piece, int x, int y, int ix, int iy, int limit, square_set_t* squares);

  char square[8][8];
  Move last_move;
  int move_num;
  player_t move_player;
};

class Game {
public:
  Game() { reset(); }
  bool add_move(int num, player_t player, cstr move);
  void add_move(cstr move);
  void take_back();
  void reset();
  const Position& pos() const { return positions.back(); }
  const Move& get_last_move() const { return pos().get_last_move(); }
  int get_move_num() const { return pos().get_move_num(); }
  player_t get_move_player() const { return pos().get_move_player(); }

private:
  std::vector<Position> positions;
};

class Novag {
public:
  Novag(Serial& s) : serial(s), referee_mode(false), notation(NotationDefault), state(NORMAL) {}
  void start();
  void set_referee_mode(bool on) { referee_mode = on; }
  void set_notation(Notation n) { notation = n; }
  Notation get_notation() const { return notation; }
  void received_line(cstr line);
  void reset_game();
  void send_move(cstr move, bool blink = true);
  void send_moves(const std::vector<std::string>& moves);

private:
  typedef enum { NORMAL, RECV_GAME_HDR, RECV_GAME } state_t;

  void query_game();
  void received_move(cstr line);
  void game_line(cstr line);
  bool add_move(int num, player_t player, cstr move);
  std::string move_convert(std::string move);

  Serial& serial;
  Game game;
  bool referee_mode;
  Notation notation;
  state_t state;
  int expected_moves;
};

void dbg_out(cstr msg);
void err_out(cstr msg);
void tokenize(cstr s, std::vector<std::string>* tkns);

#endif
