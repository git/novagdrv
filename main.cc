/* main.cc -- main module
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "novag.h"

#include <fstream>
#include <sstream>
#include <sys/select.h>
#include <vector>

using namespace std;

static int debug = 0;

static Serial serial;
static Novag novag(serial);

static struct config_t { string device; } config;

static std::string read_line() {
  std::string line;
  getline(cin, line);
  if (cin.eof()) {
    dbg_out("EOF on stdin. Closing...");
    exit(0);
  }
  return line;
}

static void dispatch() {
  fd_set rfds;
  int retval;
  int fd = serial.get_fd();

  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  FD_SET(fd, &rfds);
  retval = select(fd + 1, &rfds, NULL, NULL, NULL);
  if (retval > 0) {
    if (FD_ISSET(0, &rfds)) {
      dbg_out("Stdin input...");
      string line = read_line();
      if (line == "CLOSE") {
        dbg_out("Closing...");
        exit(0);
      }
      else if (line == "SET_GAME" || line == "MOVES") {
        if (line == "SET_GAME") {
          dbg_out("Setting game...");
          novag.reset_game();
        }
        else {
          dbg_out("Forwarding batch of moves...");
        }
        std::vector<std::string> moves;
        while (true) {
          getline(cin, line);
          if (line == "END")
            break;
          moves.push_back(line);
        }
        novag.send_moves(moves);
      }
      else {
        novag.send_move(line);
      }
    }
    else {
      std::string line;
      serial.recv(&line);
      novag.received_line(line);
    }
  }
  else {
    perror("select");
    err_out("SELECT ERROR");
    exit(1);
  }
}

static void usage() {
  cout << "\nOptions: " << endl
       << endl
       << "\t-d           enable debug output\n"
       << "\t-u           set referee mode (human vs human)\n"
       << "\t-U           write moves in UCI notation\n"
       << "\t-c           write castles as two-square king moves\n"
       << "\t-f <device>  set device (default: /dev/ttyS0)" << endl
       << endl;
}

static void read_config() {
  char* home = getenv("HOME");
  if (!home)
    return;
  ifstream f(string(string(home) + "/.novagdrvrc").c_str());
  if (!f)
    return;
  string s;
  while (getline(f, s)) {
    string::size_type p = s.find("=");
    if (p == string::npos)
      continue;
    string opt, val;
    opt = s.substr(0, p);
    val = s.substr(p + 1);
    if (opt == "device")
      config.device = val;
    else
      cerr << "Unknown option: " << '"' << opt << '"' << endl;
  }
}

int main(int argc, char* argv[]) try {
  read_config();
  if (config.device.empty())
    config.device = "/dev/ttyS0";

  for (int ac = 1; ac < argc; ac++) {
    string as(argv[ac]);
    if (as == "-h" || as == "-help" || as == "--help") {
      usage();
      return 0;
    }
    else if (as == "-d") {
      debug++;
    }
    else if (as == "-u") {
      novag.set_referee_mode(true);
      dbg_out("referee mode");
    }
    else if (as == "-U") {
      novag.set_notation(NotationUCI);
      dbg_out("UCI mode");
    }
    else if (as == "-c" && novag.get_notation() != NotationUCI) {
      novag.set_notation(NotationDefaultNoCastle);
      dbg_out("castle conversion");
    }
    else if (as == "-f" && (ac + 1) < argc) {
      config.device = argv[++ac];
    }
  }

  serial.open(config.device, B57600);
  novag.start();

  dbg_out("Novag driver ready...");

  while (1) {
    dispatch();
  }
} catch (exception& e) {
  err_out(e.what());
  exit(1);
}

void dbg_out(cstr msg) {
  if (debug)
    cerr << "# " << msg << endl;
}

void err_out(cstr msg) {
  cerr << "!!! " << msg << endl;
}

void tokenize(cstr s, vector<string>* tkns) {
  string buf;
  stringstream ss(s);

  while (ss >> buf) {
    tkns->push_back(buf);
  }
}
