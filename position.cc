/* position.cc -- position tracking
   Copyright (C) 2007 Maximiliano Pin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "novag.h"

#include <cctype>
#include <cstring>

using namespace std;

// clang-format off
static char ini_pos[8][8] =
	{ { 'R', 'P', ' ', ' ', ' ', ' ', 'p', 'r' },
	  { 'N', 'P', ' ', ' ', ' ', ' ', 'p', 'n' },
	  { 'B', 'P', ' ', ' ', ' ', ' ', 'p', 'b' },
	  { 'Q', 'P', ' ', ' ', ' ', ' ', 'p', 'q' },
	  { 'K', 'P', ' ', ' ', ' ', ' ', 'p', 'k' },
	  { 'B', 'P', ' ', ' ', ' ', ' ', 'p', 'b' },
	  { 'N', 'P', ' ', ' ', ' ', ' ', 'p', 'n' },
	  { 'R', 'P', ' ', ' ', ' ', ' ', 'p', 'r' } };
// clang-format on

Position::Position() {
  memcpy(square, ini_pos, sizeof(square));
  move_num = 0;
  move_player = BLACK;
}

Position::Position(const Position& prev, cstr move) {
  memcpy(square, prev.square, sizeof(square));
  move_num = prev.move_num;
  move_player = !prev.move_player;
  if (move_player == WHITE)
    move_num++;

  decode_move(move, &last_move);
  dbg_out("DECODED:  " + last_move.to_string(NotationNovagWithPiece));
  deduce_move(&last_move);
  dbg_out("DEDUCED:  " + last_move.to_string(NotationNovagWithPiece));

  if (last_move.short_castle) {
    square[4][last_move.oy] = square[7][last_move.oy] = ' ';
    square[5][last_move.oy] = (move_player == BLACK) ? 'r' : 'R';
    square[6][last_move.oy] = (move_player == BLACK) ? 'k' : 'K';
  }
  else if (last_move.long_castle) {
    square[4][last_move.oy] = square[0][last_move.oy] = ' ';
    square[3][last_move.oy] = (move_player == BLACK) ? 'r' : 'R';
    square[2][last_move.oy] = (move_player == BLACK) ? 'k' : 'K';
  }
  else if (last_move.ox >= 0 && last_move.oy >= 0) {
    if (last_move.promote == ' ') {
      square[last_move.dx][last_move.dy] = square[last_move.ox][last_move.oy];
    }
    else {
      square[last_move.dx][last_move.dy] =
          (move_player == BLACK) ? tolower(last_move.promote) : last_move.promote;
    }
    square[last_move.ox][last_move.oy] = ' ';
    if (last_move.ep) {
      square[last_move.dx][last_move.oy] = ' ';
    }
  }
  else {
    err_out("I don't understand: " + move);
  }

  dbg_out("--- " + last_move.to_string(NotationNovag));
  print();
  dbg_out("---");
}

void Position::print() const {
  for (int y = 7; y >= 0; y--) {
    string l;
    for (int x = 0; x < 8; x++) {
      l += ' ';
      l += square[x][y];
    }
    dbg_out(l);
  }
}

void Position::decode_move(cstr move, Move* smove) {
  const char* p = move.c_str();
  if (move == "O-O" || move == "o-o")
    smove->short_castle = true;
  if (move == "O-O-O" || move == "o-o-o")
    smove->long_castle = true;
  if (smove->short_castle || smove->long_castle) {
    smove->piece = 'K';
    smove->ox = 4;
    smove->dx = smove->short_castle ? 6 : 2;
    smove->oy = smove->dy = (move_player == BLACK) ? 7 : 0;
    return;
  }
  if (isupper(*p)) {
    smove->piece = *p++;
  }
  if (*p != '-' && *p != 'x') {
    int col = -1, row = -1;
    if (isalpha(*p)) {
      col = *p++ - 'a';
    }
    if (isdigit(*p)) {
      row = *p++ - '1';
    }
    if (*p == '-' || *p == 'x' || isalpha(*p) && !isupper(*p)) {
      smove->ox = col;
      smove->oy = row;
    }
    else {
      smove->dx = col;
      smove->dy = row;
    }
  }
  if (*p == '-' || *p == 'x') {
    p++;
  }
  if (isalpha(*p) && !isupper(*p)) {
    smove->dx = *p++ - 'a';
    smove->dy = *p++ - '1';
  }
  if (*p == '=' || *p == '/') {
    p++;
  }
  if (*p && *p != 'e') {
    if (smove->piece == ' ')
      smove->piece = 'P';
    smove->promote = toupper(*p);
  }
  if (smove->piece == ' ' && smove->oy < 0)
    smove->piece = 'P';
}

void Position::deduce_move(Move* smove) {
  if (smove->ox < 0 || smove->oy < 0) {
    square_set_t origs;
    gen_origs(smove->piece, smove->piece, smove->dx, smove->dy, &origs);

    square_set_t::iterator it;
    for (it = origs.begin(); it != origs.end(); it++) {
      if (!(smove->ox >= 0 && it->first != smove->ox ||
            smove->oy >= 0 && it->second != smove->oy)) {
        break;
      }
    }
    if (it != origs.end()) {
      smove->ox = it->first;
      smove->oy = it->second;
    }
  }
  if (smove->ox >= 0 && smove->oy >= 0) {
    smove->piece = toupper(square[smove->ox][smove->oy]);
    smove->take = (square[smove->dx][smove->dy] != ' ');
    if (!smove->take && smove->piece == 'P' && smove->ox != smove->dx) {
      smove->ep = true;
      smove->take = true;
    }
  }
  if (smove->piece == 'P' && (smove->dy == 0 || smove->dy == 7) && smove->promote == ' ') {
    smove->promote = 'Q';
  }
}

void Position::gen_origs(char piece, char rpiece, int x, int y, square_set_t* origs) {
  if (piece == 'Q' || piece == 'K') {
    gen_origs('R', piece, x, y, origs);
    gen_origs('B', piece, x, y, origs);
    return;
  }

  if (move_player == BLACK)
    rpiece = tolower(rpiece);

  int i;
  switch (piece) {
    case 'R':
      find(rpiece, x, y, -1, 0, 8, origs);
      find(rpiece, x, y, 1, 0, 8, origs);
      find(rpiece, x, y, 0, -1, 8, origs);
      find(rpiece, x, y, 0, 1, 8, origs);
      break;
    case 'B':
      find(rpiece, x, y, -1, -1, 8, origs);
      find(rpiece, x, y, -1, 1, 8, origs);
      find(rpiece, x, y, 1, -1, 8, origs);
      find(rpiece, x, y, 1, 1, 8, origs);
      break;
    case 'N':
      find(rpiece, x, y, 1, 2, 1, origs);
      find(rpiece, x, y, 2, 1, 1, origs);
      find(rpiece, x, y, 2, -1, 1, origs);
      find(rpiece, x, y, 1, -2, 1, origs);
      find(rpiece, x, y, -1, -2, 1, origs);
      find(rpiece, x, y, -2, -1, 1, origs);
      find(rpiece, x, y, -2, 1, 1, origs);
      find(rpiece, x, y, -1, 2, 1, origs);
      break;
    case 'P':
      int dir = (move_player == BLACK) ? 1 : -1;
      find(rpiece, x, y, 0, dir, 2, origs);
      find(rpiece, x, y, -1, dir, 1, origs);
      find(rpiece, x, y, 1, dir, 1, origs);
      break;
  }
}

void Position::find(char piece, int x, int y, int ix, int iy, int limit, square_set_t* squares) {
  while (limit--) {
    x += ix;
    y += iy;
    if (x < 0 || x > 7 || y < 0 || y > 7) {
      break;
    }
    if (square[x][y] == piece) {
      squares->push_back(make_pair(x, y));
      break;
    }
    if (square[x][y] != ' ')
      break;
  }
}

string Move::to_string(Notation notation) const {
  string s;

  const bool std_castle =
      (notation == NotationNovag || notation == NotationNovagWithPiece ||
       notation == NotationDefault);
  const bool include_piece = (notation == NotationNovagWithPiece);
  const bool novag =
      (notation == NotationNovag || notation == NotationNovagNoCastle ||
       notation == NotationNovagWithPiece);
  const bool uci = (notation == NotationUCI);

  if (std_castle) {
    if (short_castle) {
      s = "O-O";
      return s;
    }
    if (long_castle) {
      s = "O-O-O";
      return s;
    }
  }
  if (include_piece)
    s = piece;
  if (ox >= 0)
    s += ('a' + ox);
  if (oy >= 0)
    s += ('1' + oy);
  if (!uci)
    s += (take ? 'x' : '-');
  s += ('a' + dx);
  s += ('1' + dy);
  if (uci) {
    s += tolower(promote);
  }
  else {
    if (promote != ' ') {
      s += novag ? '/' : '=';
      s += promote;
    }
    if (ep) {
      s += "ep";
    }
  }
  return s;
}
